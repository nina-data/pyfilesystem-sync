DEPRECATED: use `rclone`


# Installation

```
poetry install --no-dev --no-root
```

# Usage

Source and destination should be specified accordingly to Refer to https://docs.pyfilesystem.org/en/latest/openers.html#format.

## CLI

```
poetry run ./pyfilesystem-sync.py --help
```

## HTTP

```
poetry run hug -f pyfilesystem-sync.py
```
