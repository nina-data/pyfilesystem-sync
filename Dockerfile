FROM python:3.8-slim-buster

WORKDIR /app

# https://github.com/python-poetry/poetry/issues/3153#issuecomment-816254484
RUN apt-get update && apt-get install -y git
RUN pip install git+https://github.com/python-poetry/poetry.git@master

RUN poetry config virtualenvs.create true
COPY pyproject.toml poetry.lock ./
RUN poetry install --no-root --no-dev

COPY pyfilesystem-sync.py .
EXPOSE 8000
CMD exec poetry run hug -f pyfilesystem-sync.py
