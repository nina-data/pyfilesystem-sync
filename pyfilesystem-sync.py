#!/usr/bin/env python3

import fs
import fs.copy
import fs.errors
import hug


@hug.type(extend=hug.types.text)
def open_fs(value):
    return fs.open_fs(value)


@hug.type(extend=hug.types.multiple)
def multiple_or_null(value):
    if len(value) > 0:
        return value
    else:
        return None


def on_copy_generator(delete_original_files):
    if delete_original_files:

        def on_copy(src_fs, src_path, dst_fs, dst_path):
            src_fs.remove(src_path)
            parent = fs.path.dirname(src_path)
            if src_fs.isempty(parent):
                try:
                    src_fs.removedir(parent)
                except fs.errors.RemoveRootError:
                    pass

    else:

        def on_copy(src_fs, src_path, dst_fs, dst_path):
            pass

    return on_copy


@hug.get("/sync")
@hug.cli()
def sync(
    src_fs: open_fs,
    dst_fs: open_fs,
    match: multiple_or_null = [],
    exclude: multiple_or_null = [],
    condition: hug.types.one_of(
        ["always", "newer", "older", "exists", "not_exists"]
    ) = "always",
    delete_original_files: hug.types.boolean = False,
):
    """ Copy or move a set of files """
    walker = fs.walk.Walker(filter=match, exclude=exclude)
    on_copy = on_copy_generator(delete_original_files)
    fs.copy.copy_dir_if(src_fs, ".", dst_fs, ".", condition, walker, on_copy=on_copy)


if __name__ == "__main__":
    sync.interface.cli()
